#include "parser.h"
#include <iostream>
#include <memory>

std::nullptr_t error(const std::string &msg)
{
    std::cerr << "error: " << msg << std::endl;
    return nullptr;
}

Parser::Parser(Lexer &lexer)
    : lexer_(lexer) {}

int Parser::cur_token() { return cur_token_; }

int Parser::get_next_token()
{
    return (cur_token_ = lexer_.get_token());
}

/// expression
///   ::= primary binop_rhs
std::unique_ptr<ExprAST> Parser::parse_expr()
{
    std::unique_ptr<ExprAST> lhs = parse_primary_expr();
    if (!lhs) return nullptr;

    return parse_binop_rhs(0, std::move(lhs));
}

/// binop_rhs
///   ::= ('+' primary)*
std::unique_ptr<ExprAST>
    Parser::parse_binop_rhs(int expr_prec,
                            std::unique_ptr<ExprAST> lhs)
{
    // if this is a binary op, find its precedence
    while (true) {
        int prec = get_token_precedence();
        if (prec < -1) {
            return lhs;
        }

        // if this is a binary operator that binds at least as tightly as the
        // current binary operator, consume it, otherwise we're done.
        if (prec < expr_prec) {
            return lhs;
        }

        // now, we know this a binary operator
        int binop = cur_token_;
        get_next_token(); // eat binop

        // parse the primary expression after the binary operator
        auto rhs = parse_primary_expr();
        if (!rhs) return nullptr;

        // if binary operator binds less tightly with rhs than the operator
        // after rhs, let the pending operator take rhs at its lhs
        int next_prec = get_token_precedence();
        if (prec < next_prec) {
            rhs = parse_binop_rhs(prec + 1, std::move(rhs));
            if (!rhs) return nullptr;
        }

        // marge lhs and rhs
        lhs = std::make_unique<BinaryExprAST>(binop,
                                              std::move(lhs),
                                              std::move(rhs));
    }
}

/// prototype
///   ::= id '(' id* ')'
std::unique_ptr<PrototypeAST> Parser::parse_prototype()
{
    if (cur_token_ != tok_identifier) {
        return error("expected function name in prototype");
    }

    std::string name = lexer_.identifier_str();

    get_next_token();
    if (cur_token_ != '(') {
        return error("expected '(' in prototype");
    }

    // read the list of argument names
    std::vector<std::string> arg_names;
    while (get_next_token() == tok_identifier) {
        arg_names.push_back(lexer_.identifier_str());
    }

    if (cur_token_ != ')') {
        return error("expected ')' in prototype");
    }

    get_next_token(); // eat ')'

    return std::make_unique<PrototypeAST>(name, arg_names);
}

/// definition ::= 'def' prototype expression
std::unique_ptr<FunctionAST> Parser::parse_definition()
{
    get_next_token(); // eat 'def'
    auto prototype = parse_prototype();
    if (!prototype) return nullptr;

    auto expr = parse_expr();
    if (!expr) return nullptr;

    return std::make_unique<FunctionAST>(std::move(prototype), std::move(expr));
}

/// external ::= 'extern' prototype
std::unique_ptr<PrototypeAST> Parser::parse_extern()
{
    get_next_token(); // eat 'extern'
    return parse_prototype();
}

/// toplevelexpr ::= expression
std::unique_ptr<FunctionAST> Parser::parse_root_expr()
{
    auto expr = parse_expr();
    if (!expr) return nullptr;

    // make an anonymous prototype
    auto prototype =
        std::make_unique<PrototypeAST>("", std::vector<std::string>());

    return std::make_unique<FunctionAST>(std::move(prototype), std::move(expr));
}

std::unique_ptr<ExprAST> Parser::parse_number_expr()
{
//    get_next_token();

    std::unique_ptr<ExprAST> result =
        std::make_unique<NumberExprAST>(lexer_.num_val());

    return result;
}

/// parenexpr ::= '(' expression ')'
std::unique_ptr<ExprAST> Parser::parse_paren_expr()
{
    get_next_token(); // skip '('
    auto expr = parse_expr();
    if (!expr) return nullptr;

    if (cur_token_ != ')') {
        return error("expected ')'");
    }

    get_next_token(); // skip ')'
    return expr;
}

/// identifierexpr
///   ::= identifier
///   ::= identifier '(' expression* ')'
std::unique_ptr<ExprAST> Parser::parse_ident_expr()
{
    std::string name = lexer_.identifier_str();
    get_next_token(); // eat identifier

    if (cur_token_ != '(') {
        // variable reference
        return std::make_unique<VariableExprAST>(name);
    }

    // function call
    get_next_token(); // eat '('
    std::vector<std::unique_ptr<ExprAST>> args;
    if (cur_token_ != ')') {
        while (true) {
            auto arg = parse_expr();
            if (!arg) return nullptr;
            args.push_back(std::move(arg));

            if (cur_token_ == ')')
                break;

            if (cur_token_ != ',') {
                return error("Expected ')' or ',' in argument list");
            }

            get_next_token();
        }
    }

    get_next_token(); // eat ')'

    return std::make_unique<CallExprAST>(name, std::move(args));
}

/// primary
///   ::= identifierexpr
///   ::= numberexpr
///   ::= parenexpr
std::unique_ptr<ExprAST> Parser::parse_primary_expr()
{
    switch (cur_token_) {
    case tok_identifier: return parse_ident_expr();
    case tok_number: return parse_number_expr();
    case '(': return parse_paren_expr();
    default: return error("unknown token when expecting an expression");
    }
}

void Parser::init_binop_precendence()
{
    // 1 is the lowest precedence
    binop_precedence_ = {
        {',', 10},
        {'+', 20},
        {'-', 20},
        {'*', 40} // highest
    };
}

// TODO: this should really be an `optional<int>`
int Parser::get_token_precedence()
{
    if (!isascii(cur_token_)) {
        return -1;
    }

    int prec = binop_precedence_[cur_token_];
    if (prec <= 0) return -1;
    return prec;
}


