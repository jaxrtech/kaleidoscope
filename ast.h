
#ifndef KALEIDOSCOPE_AST_H
#define KALEIDOSCOPE_AST_H

#include <string>
#include <vector>
#include <memory>

/// ExprAST - Base class for all expression nodes.
class ExprAST {
public:
    virtual ~ExprAST();
};

/// NumberExprAST - Expression class for numeric literals like "1.0"
class NumberExprAST : public ExprAST {
public:
    NumberExprAST(double val);

private:
    double val_;
};

/// VariableExprAST - Expression class for referencing a variable, like "a"
class VariableExprAST : public ExprAST {
public:
    VariableExprAST(const std::string &name);

private:
    std::string name_;
};

/// BinaryExprAst - Expression class for a binary operator
class BinaryExprAST : public ExprAST {
public:
    BinaryExprAST(char op,
                  std::unique_ptr<ExprAST> lhs,
                  std::unique_ptr<ExprAST> rhs);

private:
    char op_;
    std::unique_ptr<ExprAST> lhs_, rhs_;
};

/// CallExperAST - Expression class for function calls.
class CallExprAST : public ExprAST {
public:
    CallExprAST(const std::string &callee,
                std::vector<std::unique_ptr<ExprAST>> args);

private:
    std::string callee_;
    std::vector<std::unique_ptr<ExprAST>> args_;
};

/// PrototypeAST - This class represents the "prototype" for a function which
/// captures its name, and its argument names (thus implicitly the number of
/// arguments te function takes).
class PrototypeAST {
public:
    PrototypeAST(const std::string &name, const std::vector<std::string> &args);

private:
    std::string name_;
    std::vector<std::string> args_;
};


/// FunctionAST - This class represents a function definition itself.
class FunctionAST {
public:
    FunctionAST(std::unique_ptr<PrototypeAST> proto,
                std::unique_ptr<ExprAST> body);

private:
    std::unique_ptr<PrototypeAST> proto_;
    std::unique_ptr<ExprAST> body_;
};

#endif //KALEIDOSCOPE_AST_H
