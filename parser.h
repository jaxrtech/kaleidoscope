#ifndef KALEIDOSCOPE_PARSER_H
#define KALEIDOSCOPE_PARSER_H

#include <map>
#include "lexer.h"
#include "ast.h"

class Parser {
public:
    Parser(Lexer &lexer);

    /// cur_tok_/get_next_token_ - Provide a simple token buffer. `cur_tok` is
    /// the current token the parser is looking at. `get_next_token` reads
    /// another token from the lexer updates `cur_tok` with its results.
    int cur_token();
    int get_next_token();

    std::unique_ptr<FunctionAST> parse_definition();
    std::unique_ptr<PrototypeAST> parse_extern();
    std::unique_ptr<FunctionAST> parse_root_expr();

private:
    Lexer &lexer_;

    /// binop_precedence_ - holds the precedence for each binary operator
    /// that is defined.
    std::map<char, int> binop_precedence_;

    /// init_binop_precendence - adds the binary operators to the precedence
    /// table in `binop_precedence_`
    void init_binop_precendence();

    /// get_token_precedence - get the precedence fo the pending binary
    /// operator token.
    int get_token_precedence();

    int cur_token_;

    std::unique_ptr<ExprAST> parse_expr();
    std::unique_ptr<ExprAST> parse_number_expr();
    std::unique_ptr<ExprAST> parse_paren_expr();
    std::unique_ptr<ExprAST> parse_ident_expr();
    std::unique_ptr<ExprAST> parse_primary_expr();
    std::unique_ptr<ExprAST> parse_binop_rhs(int expr_prec,
                                             std::unique_ptr<ExprAST> lhs);
    std::unique_ptr<PrototypeAST> parse_prototype();
};

#endif //KALEIDOSCOPE_PARSER_H
