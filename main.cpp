#include <iostream>
#include "lexer.h"
#include "parser.h"

static void handle_definition(Parser &parser)
{
    auto def = parser.parse_definition();
    if (def) {
        std::cout << "Parsed a function definition." << std::endl;
    }
}

static void handle_extern(Parser &parser)
{
    auto def = parser.parse_extern();
    if (def) {
        std::cout << "Parsed a extern function definition." << std::endl;
    }
}

static void handle_root_expr(Parser &parser)
{
    auto def = parser.parse_root_expr();
    if (def) {
        std::cout << "Parsed a top-level expression." << std::endl;
    }
}

/// top ::= definition | external | expression | ';'
bool repl(Lexer &lexer, Parser &parser)
{
    std::cout << "kl> ";
    while (true) {
        parser.get_next_token();
        switch (parser.cur_token()) {
        case tok_eof:
            return false;

        case ';':
            // ignore top-level semicolons
            parser.get_next_token();
            continue;

        case tok_def:
            handle_definition(parser);
            continue;

        default:
            handle_root_expr(parser);
            continue;
        }
    }

    return true;
}

int main()
{
    Lexer lexer;
    Parser parser(lexer);
    while (repl(lexer, parser));
    return 0;
}