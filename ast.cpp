//
// Created by josh on 3/12/17.
//

#include "ast.h"

ExprAST::~ExprAST() {}

NumberExprAST::NumberExprAST(double val)
    : val_(val) {}

VariableExprAST::VariableExprAST(const std::string &name) {}

BinaryExprAST::BinaryExprAST(char op,
                             std::unique_ptr<ExprAST> lhs,
                             std::unique_ptr<ExprAST> rhs)
    : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

CallExprAST::CallExprAST(const std::string &callee,
                         std::vector<std::unique_ptr<ExprAST>> args)
    : callee_(callee), args_(std::move(args)) {}

PrototypeAST::PrototypeAST(const std::string &name,
                           const std::vector<std::string> &args)
    : name_(name), args_(args) {}

FunctionAST::FunctionAST(std::unique_ptr<PrototypeAST> proto,
                         std::unique_ptr<ExprAST> body)
    : proto_(std::move(proto)), body_(std::move(body)) {}
