#include "lexer.h"

Lexer::Lexer() { }

int Lexer::get_token() {
    static int last_char = ' ';

    // skip whitespace
    while (isspace(last_char)) {
        last_char = getchar();
    }

    // identifier: [a-zA-Z][a-zA-Z0-9]*
    if (isalpha(last_char)) {
        identifier_str_ = last_char;
        while (isalnum((last_char = getchar()))) {
            identifier_str_ += last_char;
        }

        if (identifier_str_ == "def")
            return tok_def;
        if (identifier_str_ == "extern")
            return tok_extern;

        return tok_identifier;
    }

    // number: [0-9\.]+
    if (isdigit(last_char) || last_char == '.') {
        std::string num_str;
        do {
            num_str += last_char;
            last_char = getchar();
        } while (isdigit(last_char) || last_char == '.');

        num_val_ = strtod(num_str.c_str(), 0);
        return tok_number;
    }

    // comment until end of line
    if (last_char == '#') {
        do {
            last_char = getchar();
        } while (last_char != EOF && last_char != '\n' && last_char != '\r');

        if (last_char != EOF) {
            return get_token();
        }
    }

    // check of eof and don't eat it
    if (last_char == EOF) {
        return tok_eof;
    }

    // otherwise, just return the character as its ascii value
    int cur_char = last_char;
    last_char = getchar();
    return cur_char;
}

std::string Lexer::identifier_str() { return identifier_str_; }

double Lexer::num_val() { return num_val_; }

