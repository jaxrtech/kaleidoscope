cmake_minimum_required(VERSION 3.6)
project(kaleidoscope)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp lexer.cpp lexer.h ast.cpp ast.h parser.cpp parser.h)
add_executable(kaleidoscope ${SOURCE_FILES})