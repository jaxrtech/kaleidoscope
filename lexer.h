
#ifndef KALEIDOSCOPE_LEXER_H
#define KALEIDOSCOPE_LEXER_H

#include <string>

// The lexer returns tokens [0-255] if it is an unknown character, otherwise
// a known token
enum Token {
    tok_eof = -1,

    // commands
    tok_def = -2, tok_extern = -3,

    // primary
    tok_identifier = -4, tok_number = -5,
};

class Lexer {
public:
    Lexer();
    int get_token();

    std::string identifier_str();
    double num_val();

private:
    std::string identifier_str_; // filled in if `tok_identifier`
    double num_val_; // filled in if `tok_number`
};

#endif //KALEIDOSCOPE_LEXER_H
